import { isUndefined, isNull } from 'util';

export function incrementString (value: string): string {
  const match = value.match(/([a-zA-Z]*)(\d+)$/);
  const incrementedValue: string =
    match && !isUndefined(match.length) ? (Number(match[2]) + 1).toString().padStart(match[2].length, '0') : '1';
  return isNull(match) ? value.concat(incrementedValue) : match[1].concat(incrementedValue);
}

export function incrementStringSingleStatement (value: string): string {
  return isNull(value.match(/([a-zA-Z]*)(\d+)$/))
    ? value.concat('1')
    : (value.match(/([a-zA-Z]*)(\d+)$/) as RegExpMatchArray)[1].concat(
        value.match(/([a-zA-Z]*)(\d+)$/) && !isUndefined((value.match(/([a-zA-Z]*)(\d+)$/) as RegExpMatchArray).length)
          ? (Number((value.match(/([a-zA-Z]*)(\d+)$/) as RegExpMatchArray)[2]) + 1)
              .toString()
              .padStart((value.match(/([a-zA-Z]*)(\d+)$/) as RegExpMatchArray)[2].length, '0')
          : '1'
      );
}
