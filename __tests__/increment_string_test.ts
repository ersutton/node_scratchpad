import { incrementStringSingleStatement } from '../src';

describe('authenticate tests', () => {
  test('_authenticateValidation tests ', () => {
    expect(incrementStringSingleStatement('foo')).toEqual('foo1');
    expect(incrementStringSingleStatement('foobar23')).toEqual('foobar24');
    expect(incrementStringSingleStatement('foo0042')).toEqual('foo0043');
    expect(incrementStringSingleStatement('foo9')).toEqual('foo10');
    expect(incrementStringSingleStatement('foo099')).toEqual('foo100');
  });
});
